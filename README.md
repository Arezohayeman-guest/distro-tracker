# distro-tracker

distro-tracker is a set of services tailored to distribution developers,
package maintainers, and anybody who might have to interact with those
people (upstream developers, bug reporters, advanced users, etc). It lets
you follow almost everything related to the life of a package (or of a set
of packages).

## Documentation

The [documentation](https://tracker.debian.org/docs/) is updated once a
day and matches what is deployed on tracker.debian.org.

Otherwise you can generate the documentation yourself by doing `make html`
in the docs subdirectory of the distro-tracker git repository.

## Interacting with the project

### How to contribute

Have a look at the ["Contributing"
section](https://tracker.debian.org/docs/contributing.html) of the
documentation.

### Contact information

You can interact with the developers on the debian-qa@lists.debian.org
mailing list ([archive](https://lists.debian.org/debian-qa/)) or on
the `#debian-qa` IRC channel on the OFTC network (irc.debian.org server
for example).

The lead developer is Raphaël Hertzog (buxy on IRC).

### Reporting bugs and vulnerabilities

We use the Debian bug tracker to manage bug reports (including security
issues, there's a "security" tag). If you have something to report, please
file a bug against the `tracker.debian.org` package. You can follow
[Debian's instructions](https://www.debian.org/Bugs/Reporting) to complete
this process. You should check the [list of open
issues](https://bugs.debian.org/tracker.debian.org) before filing a new
bug to avoid duplicates.

You can also have a look at all the [closed bug
reports](https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=1;package=tracker.debian.org) too.

## Misc information

### Badges

[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1440/badge)](https://bestpractices.coreinfrastructure.org/projects/1440)

[![Code Health](https://landscape.io/github/rhertzog/distro-tracker/master/landscape.svg?style=flat)](https://landscape.io/github/rhertzog/distro-tracker/master)

### Known distro-tracker deployments

* https://tracker.debian.org
* http://pkg.kali.org


